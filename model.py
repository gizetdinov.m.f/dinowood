import torch
from torch import optim, nn
import lightning as L
from transformers import AutoImageProcessor, AutoModel



# define the LightningModule
class DinoWoodModel(L.LightningModule):
    def __init__(self):
        super().__init__()
        self.backbone = AutoModel.from_pretrained('facebook/dinov2-small')
        self.segmentator = nn.Conv2d(384, 10, 1)
        self.loss = nn.CrossEntropyLoss()
    
    def forward(self, x):
        height, width = x.shape[2]//14, x.shape[3]//14
        features = self.backbone(x)
        features = features.last_hidden_state
        features = features[:, 1:, :]
        features = features.reshape(-1, height, width, 384)
        features = features.permute(0,3,1,2)
        return self.segmentator(features)

    def training_step(self, batch, batch_idx):
        # training_step defines the train loop.
        # it is independent of forward
        img, target = batch

        pred = self.forward(img)

        target = torch.nn.functional.interpolate(target, size=pred.shape[2:])
        
        loss = self.loss(pred, target)
        # Logging to TensorBoard (if installed) by default
        self.log("train_loss", loss)
        return loss

    def configure_optimizers(self):
        optimizer = optim.Adam(self.segmentator.parameters(), lr=1e-3)
        return optimizer


# init the autoencoder
dinowood_model = DinoWoodModel()
