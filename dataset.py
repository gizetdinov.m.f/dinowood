import os
import torch
from torch import utils
from torch.utils.data import Dataset, DataLoader
from torchvision.transforms import ToTensor
from PIL import Image
import numpy as np

train_bs = 1
val_bs = 1

class WoodDataset(Dataset):
    def __init__(self, root, subset, transforms = None) -> None:
        super().__init__()
        self.root = root
        self.subset = subset
        self.imgs = os.listdir(root+subset)
        from PIL import ImageColor
        

        map_specification = open(root+'Semantic Map Specification.txt', "r")

        name=''
        color=''
        i = 0
        self.map_dir = {}
        for x in map_specification:
            if 'name' in x:
                name = x[5:-1]
            if 'color' in x:
                color = x[6:]
                if color != '':
                    self.map_dir[i] = ImageColor.getcolor(f'#{color}', "RGB")
                    i+=1
        
        self.transforms = transforms

    def __len__(self):
        return len(self.imgs)
    
    def rgb_to_mask(self, img):
        num_classes = len(self.map_dir)
        shape = img.shape[:2]+(num_classes,)
        out = np.zeros(shape, dtype=np.float64)
        for i, cls in enumerate(self.map_dir):
            out[:,:,i] = np.all(np.array(img).reshape( (-1,3) ) == self.map_dir[i], axis=1).reshape(shape[:2])
        return out.transpose(2,0,1)

    def mask_to_rgb(mask, color_map):
        single_layer = np.argmax(mask, axis=1)
        output = np.zeros((mask.shape[0],mask.shape[2],mask.shape[3],3))
        for k in color_map.keys():
            output[single_layer==k] = color_map[k]
        return np.uint8(output)

    def __getitem__(self, index):
        img_path = self.imgs[index]
        mask_path = img_path[:-4] + '_segm.bmp'

        img, mask = (
            np.array(Image.open(self.root + f'{self.subset}/'+ img_path)),
            np.array(Image.open(self.root + 'masks/'+ mask_path))
        )

        if self.transforms is not None:
            transform_res = self.transforms(img=img, mask=mask)
            img, mask = transform_res['image'], transform_res['mask']

        mask = self.rgb_to_mask(mask)
        img, mask = torch.tensor(img).permute(-1, 0, 1), torch.tensor(mask)
        return img, mask


train_dataset = WoodDataset('/home/marat/Pictures/wood_dataset/', 'train')
val_dataset = WoodDataset('/home/marat/Pictures/wood_dataset/', 'val')

train_loader = DataLoader(train_dataset, batch_size=train_bs)
val_loader = DataLoader(val_dataset, batch_size=val_bs)

if __name__ == '__main__':
    WoodDataset('/home/marat/Pictures/wood_dataset/', 'train')[1]