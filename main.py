import lightning as L
from dataset import train_loader, val_loader
from model import dinowood_model
from lightning.pytorch.loggers import TensorBoardLogger

logger = TensorBoardLogger("tb_logs", name="DinoWood")

# train the model (hint: here are some helpful Trainer arguments for rapid idea iteration)
trainer = L.Trainer(limit_train_batches=100, max_epochs=1, logger=logger)
trainer.fit(model=dinowood_model, train_dataloaders=train_loader)
